<?php

namespace uga\hallibtt\test\selector;

use PHPUnit\Framework\TestCase;

require_once dirname(__FILE__, 3).DIRECTORY_SEPARATOR.'/vendor/autoload.php';

abstract class AbstractSelector extends TestCase {
    protected static string $classSelector;
}