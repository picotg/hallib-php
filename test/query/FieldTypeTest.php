<?php
namespace uga\hallibtt\test\query;

use PHPUnit\Framework\TestCase;
use uga\hallib\queryDefinition\FieldType;
use uga\hallib\queryDefinition\IntervalElement;
use uga\hallib\queryDefinition\LiteralElement;
use uga\hallib\search\SearchField;
use uga\hallib\search\SearchQuery;

class FieldTypeTest extends TestCase {
    public function testDateTimeConvertor () {
        $dateTimeConvertor = FieldType::getInstance('tdate')->convertors['DateTime'];
        $this->assertEquals('2023-10-01T00:00:00Z', $dateTimeConvertor::toHal(date_create('2023-10-1 00:00:00')));
        $this->assertEquals(date_create('2023-10-1 00:00:00'), $dateTimeConvertor::fromHal('2023-10-01T00:00:00Z'));
    }
    public function testFromElment() {
        $elmentTest = new LiteralElement([
            'field' => SearchField::getVarient('writingDate_tdate'),
            'convertorName' => 'DateTime'
        ]);
        $dateTimeConvertor = $elmentTest->convertor;
        $this->assertNotNull($dateTimeConvertor);
        $this->assertEquals('2023-10-01T00:00:00Z', $dateTimeConvertor::toHal(date_create('2023-10-1 00:00:00')));
        $this->assertEquals(date_create('2023-10-1 00:00:00'), $dateTimeConvertor::fromHal('2023-10-01T00:00:00Z'));
        $elmentTest->value = date_create('2023-10-1');
        $elmentTest->convertorName = null;
        $this->assertEquals('2023-10-01T00:00:00Z', $elmentTest->value);
        $elmentTest->convertorName = 'DateTime';
        $this->assertEquals(date_create('2023-10-1 00:00:00'), $elmentTest->value);
        # test même effet avec et sans convertor
        $elmentTestWithoutCOnverter = new LiteralElement([
            'field' => SearchField::getVarient('writingDate_tdate'),
            'value' => '2023-10-01T00:00:00Z'
        ]);
        $searchConverter = new SearchQuery([
            'baseQuery' => $elmentTest
        ]);
        $searchWConverter = new SearchQuery([
            'baseQuery' => $elmentTestWithoutCOnverter
        ]);
        $this->assertEquals($searchConverter->stringValue, $searchWConverter->stringValue);
    }

    public function testInterval() {
        $elmentTest = new IntervalElement([
            'field' => SearchField::getVarient('writingDate_tdate'),
            'convertorName' => 'DateTime'
        ]);
        $elmentTest->minValue = date_create('2023-10-1 00:00:00');
        $elmentTest->maxValue = date_create('2023-12-1 00:00:00');
        $this->assertEquals($elmentTest->minValue, date_create('2023-10-1 00:00:00'));
        $elmentTest->convertorName = null;
        $this->assertEquals($elmentTest->minValue, '2023-10-01T00:00:00Z');
        # test même effet avec et sans convertor
        $elmentTestWithoutCOnverter = new IntervalElement([
            'field' => SearchField::getVarient('writingDate_tdate'),
            'minValue' => '2023-10-01T00:00:00Z',
            'maxValue' => '2023-12-01T00:00:00Z',
        ]);
        $searchConverter = new SearchQuery([
            'baseQuery' => $elmentTest
        ]);
        $searchWConverter = new SearchQuery([
            'baseQuery' => $elmentTestWithoutCOnverter
        ]);
        $this->assertEquals($searchConverter->stringValue, $searchWConverter->stringValue);
    }
}
