<?php
namespace uga\hallibtt\test\query;

use uga\hallib\queryDefinition\LiteralElement;
use uga\hallib\QueryIterator;
use uga\hallib\search\SearchQuery;

final class SearchQueryTest extends AbstractQuery {
    protected static string $classQuery = SearchQuery::class;
    protected array $URL_API = [
        'simple' => 'https://api.archives-ouvertes.fr/search/?q=*&rows=5&fl=docid&sort=docid%20desc',
        'cursor' => 'https://api.archives-ouvertes.fr/search/?q=*&rows=5&fl=docid&sort=docid%20asc',
    ];
    protected static string $fieldId = 'docid';
    protected array $interval = [
        'sans DOI' => [
            'baseQuery' => [
                'field' => 'doiId_s',
                'minValue' => '""',
                'prefix' => '-',
            ],
            'sort' => 'title_sort',
            'fl' => ['docid'],
            'URL_API' => 'https://api.archives-ouvertes.fr/search/?q=-doiId_s:[%22%22%20TO%20*]&sort=title_sort%20desc&fl=docid&rows=5',
        ],
        'date' => [
            'baseQuery' => [
                'field' => 'submittedDateY_i',
                'minValue' => '2020',
                'maxValue' => '2022',
            ],
            'sort' => 'title_sort',
            'fl' => ['docid', 'abstract_s'],
            'URL_API' => 'https://api.archives-ouvertes.fr/search/?q=submittedDateY_i:[2020%20TO%202022]&sort=title_sort%20desc&fl=docid,abstract_s&rows=5'
        ]
    ];
    protected array $filterOption = [
        'submitted date' => [
            'q' => [
                'field' => 'structure_t',
                'value' => '3SR'
            ],
            'fq' => [
                'field' => 'submittedDateY_i',
                'value' => '2020'
            ],
            'sort' => 'title_sort',
            'fl' => ['docid', 'title_s'],
            'URL_API' => 'https://api.archives-ouvertes.fr/search?q=structure_t:3SR&sort=title_sort%20desc&fl=docid,title_s&rows=5&fq=submittedDateY_i:2020',
        ]
    ];

    public function testInstance() {
        $query = new static::$classQuery();
        $query->rows = 0;
        $query->baseQuery = new LiteralElement(['value' => '*']);
        $qi = new QueryIterator($query);
        $qi->rewind();
        $nbNoInstance = $qi->getNbResult();
        $query->instance = 'saga';
        $qi = new QueryIterator($query);
        $qi->rewind();
        $nbSagaInstance = $qi->getNbResult();
        $this->assertNotEquals($nbNoInstance, $nbSagaInstance, 'pas de prise en compte des portails.');
    }
}