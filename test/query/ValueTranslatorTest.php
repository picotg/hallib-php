<?php
namespace uga\hallibtt\test\query;

use PHPUnit\Framework\TestCase;
use uga\hallib\OneDocQuery;
use uga\hallib\queryDefinition\LiteralElement;
use uga\hallib\ref\author\AuthorField;
use uga\hallib\ref\author\AuthorQuery;

class AuthorFieldLocal extends AuthorField {
    public static function getDataDirectory(): string {
        return dirname(__FILE__).DIRECTORY_SEPARATOR;
    }
}

class AuthorQueryLocal extends AuthorQuery {
    protected static string $FieldClass = AuthorFieldLocal::class;
}

class ValueTranslatorTest extends TestCase {
    public function testValueTranslator() {
        AuthorFieldLocal::createList();
        $this->assertEquals(
            AuthorFieldLocal::getVarient('valid_s')->valuesTranslator,
            ['VALID' => 'PREFERRED'],
            'valuesTranslator non prise en compte',
        );
        $q = new AuthorQueryLocal([
            'rows' => 1,
        ]);
        $q->baseQuery = new LiteralElement([
            'value' => 'VALID',
            'field' => AuthorFieldLocal::getVarient('valid_s')
        ]);
        $q->addReturnedField(AuthorFieldLocal::getVarient('valid_s'));
        $q->addReturnedField(AuthorFieldLocal::getVarient('idHal_s'));
        $this->assertEquals(
            $q->stringValue,
            'https://api.archives-ouvertes.fr/ref/author?q=valid_s%3APREFERRED&wt=json&rows=1&fl=valid_s%2CidHal_s',
            'valuesTranslator non prise en compte dans la construction de la requête');
        $r1 = new OneDocQuery($q);
        $this->assertEquals(
            $r1->result->valid_s,
            'VALID',
            'valuesTranslator non prise en compte dans les valeurs de retour',
        );
    }
}
