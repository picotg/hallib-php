<?php
namespace uga\hallibtt\test\queryBuilder\ref;

use uga\hallib\ref\metadatalist\MetadatalistQuery;
use uga\hallibtt\test\query\AbstractQuery;

class MetadatalistQueryTest extends AbstractQuery {
    protected static string $classQuery = MetadatalistQuery::class;
    protected array $URL_API = [
        'simple' => 'https://api.archives-ouvertes.fr/ref/metadatalist?q=*&rows=5&fl=docid&sort=docid%20desc',
        'cursor' => 'https://api.archives-ouvertes.fr/ref/metadatalist?q=*&rows=5&fl=docid&sort=docid%20asc',
    ];
    protected static string $fieldId = 'docid';
    protected array $interval = [
        'label' => [
            'baseQuery' => [
                'field' => 'label_s',
                'minValue' => 'a',
                'maxValue' => 'd',
            ],
            'sort' => 'label_s',
            'fl' => ['docid', 'label_s'],
            'URL_API' => 'https://api.archives-ouvertes.fr/ref/metadatalist?q=label_s:[a%20TO%20d]&sort=label_s%20desc&fl=docid,label_s&rows=5'
        ]
    ];

    /**
     * @doesNotPerformAssertions
     */
    public function testFilter() {}
}
