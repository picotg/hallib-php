<?php
namespace uga\hallibtt\test\queryBuilder\ref;

use uga\hallib\ref\anrproject\ANRProjectQuery;
use uga\hallibtt\test\query\AbstractQuery;

class ANRProjectQueryTest extends AbstractQuery {
    protected static string $classQuery = ANRProjectQuery::class;
    protected array $URL_API = [
        'simple' => 'https://api.archives-ouvertes.fr/ref/anrproject?q=*&rows=5&fl=docid&sort=docid%20desc',
        'cursor' => 'https://api.archives-ouvertes.fr/ref/anrproject?q=*&rows=5&fl=docid&sort=docid%20asc',
    ];
    protected static string $fieldId = 'docid';
    protected array $interval = [
        'date' => [
            'baseQuery' => [
                'field' => 'yearDate_s',
                'minValue' => '2000',
                'maxValue' => '2020',
            ],
            'sort' => 'acronym_s',
            'fl' => ['docid', 'acronym_s'],
            'URL_API' => 'https://api.archives-ouvertes.fr/ref/anrproject?q=yearDate_s:[2000%20TO%202020]&sort=acronym_s%20desc&fl=docid,acronym_s&rows=5',
        ]
    ];
    protected array $filterOption = [
        'submitted date' => [
            'q' => [
                'field' => 'valid_s',
                'value' => 'VALID'
            ],
            'fq' => [
                'field' => 'text',
                'value' => 'da'
            ],
            'sort' => 'yearDate_s',
            'fl' => ['docid', 'title_s'],
            'URL_API' => 'https://api.archives-ouvertes.fr/ref/anrproject?q=valid_s:VALID&sort=yearDate_s%20desc&fl=docid,title_s&rows=5&fq=text:da',
        ]
    ];
}
