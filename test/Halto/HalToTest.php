<?php
namespace uga\hallibtt\test\queryBuilder\ref;

use PHPUnit\Framework\TestCase;
use uga\hallib\halto\HaltoDB;

class HalToTest extends TestCase {
    public function testSelectLine() {
        $haltoDB = new HaltoDB();
        $haltoDB->setDatabase(dirname(__FILE__).'/test.db');
        $line1 = $haltoDB->selectLine(1);
        $this->assertEquals('3SR', $line1['acronym']);
    }
}
