<?php
namespace uga\hallib;

use uga\hallib\queryDefinition\Query;
use uga\hallib\queryDefinition\Result;

/**
 * Utilitaires pour l'execution de requête sur l'API Hal et la récupératin d'un
 * seul élément.
 * 
 * @author Gaël PICOT <gael.picot@univ-grenoble-alpes.fr>
 * 
 * Hallib :
 * Copyright (C) 2022 UGA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 * 
 */

/**
 * Permet d'executé une requête et de récupérer un seul document.
 */
class OneDocQuery {
    protected Query $query;
    protected ?Result $result = null;
    protected bool $isExecuted = false;

    public function __get($name) {
        if($name === 'result') {
            return $this->getReult();
        } else {
            return null;
        }
    }

    public function getReult() {
        if(!$this->isExecuted) {
            $requestResults = new $this->query->responseClass(json_decode(file_get_contents($this->query->stringValue)));
            if(isset($requestResults->docs[0])) {
                $this->result = new Result($requestResults->docs[0], $this->query->resultFieldsTranslator, $this->query::getFieldClass());
            }
            $this->isExecuted = true;
        }
        return $this->result;
    }

    public function __construct(Query $query) {
        $this->query = $query;
        $this->query->rows = 1;
    }
}
