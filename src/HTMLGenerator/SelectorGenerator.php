<?php
namespace uga\hallib\HTMLGenerator;

/**
 * Utilitaires pour facilité la génération de html
 * 
 * @author Gaël PICOT <gael.picot@univ-grenoble-alpes.fr>
 * 
 * Hallib :
 * Copyright (C) 2022 UGA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 * 
 */

use uga\hallib\queryDefinition\DataContainer;
use uga\hallib\queryDefinition\Selector;

/**
 * Generateur de représentation sous forme de balise select en HTML d'un Selector.
 */
class SelectorGenerator extends DataContainer {
    /**
     * liste des framworks css disponible.
     */
    public const FRAMWORK_NAMES = ['Bulma', 'PlainHTML'];
    /**
     * Liste des propriété en lecture seul.
     *
     * @var array
     */
    protected static array $readOnly = ['fieldToAdd'];
    /**
     * Selecteur à représenter.
     *
     * @var Selector
     */
    protected Selector $selector;
    /**
     * Label correspondant.
     *
     * @var string
     */
    protected string $label = '';
    /**
     * nom (valeur de la propriété HTML name)
     *
     * @var string
     */
    protected string $name = '';
    /**
     * Framworks CSS utilisé (PlainHTML = aucun).
     * 
     * disponible :
     * 
     * * Bulma
     * * PlainHTML
     *
     * @var string
     */
    protected string $frameWorksName = 'PlainHTML';
    /**
     * List de choix à ajouter (au début).
     * 
     * Format : [
     *   'value' => $value,
     *   'label' => $label
     * ]
     *
     * @var array
     */
    protected array $fieldToAdd = [];
    /**
     * Valeur selectionner par defaut.
     *
     * @var string|null
     */
    protected ?string $selectedValue = null;
    /**
     * groupe les sous-élément dans des optgroup
     *
     * @var boolean
     */
    protected bool $optgroupForSub = true;
    /**
     * chatarcter débutant les sous éléments
     *
     * @var string
     */
    protected string $subCharacter = '-> ';

    /**
     * Ajoput un choix.
     *
     * @param string $value valeur (propriété value HTML)
     * @param string $label texte visible
     * @return void
     */
    public function addField(string $value, string $label) {
        array_push($this->fieldToAdd, [
                'value' => $value,
                'label' => $label
            ]
        );
    }

    /**
     * Génération récursive d'une list de tag HTML option.
     *
     * @param string|null $selectedValue valeur selectionner
     * @return string chaine contenant les option généré
     */
    public function generateOptions($values=null, $level=0) {
        if($values===null) {
            $values = array_merge($this->fieldToAdd, $this->selector->valueLabel);
        }
        $options = '';
        $prefix = '';
        if(!$this->optgroupForSub) {
            for ($i = 1; $i <= $level; $i++) {
                $prefix .= $this->subCharacter;
            }
        }
        foreach($values as $instance) {
            $options .= '<option value="'.$instance['value'].'"';
            if($this->selectedValue !== null && $this->selectedValue == $instance['value']) {
                $options .= ' selected';
            }
            $options .= '>'.$prefix.$instance['label'].'</option>';
            if(isset($instance['sub'])) {
                if($this->optgroupForSub) {
                    $options .= '<optgroup label="'.$instance['label'].'">';
                    $options .= $this->generateOptions($instance['sub'], $level+1);
                    $options .= '</optgroup>';
                }
                else {
                    $options .= $this->generateOptions($instance['sub'], $level+1);
                }
            }
        }
        return $options;
    }

    /**
     * vide la liste des choix à ajouter.
     *
     * @return void
     */
    public function emptyFieldToAdd() {
        $this->fieldToAdd = [];
    }

    /**
     * Constructeur.
     *
     * @param Selector $selector Selecteur à représenté
     */
    public function __construct(Selector $selector) {
        $this->selector = $selector;
    }

    /**
     * generateur HTML pour le framworks Bulma.
     *
     * @return string HTML généré
     */
    public function generateBulmaSelectField(): string {
        $htmlComboBox = '<label class="label" for="'.$this->name.'">'.$this->label.' </label>';
        $htmlComboBox .= '<div class="select"><select name="'.$this->name.'" id="'.$this->name.'">';
        $htmlComboBox .= $this->generateOptions();
        $htmlComboBox .= '</select></div>';
        return $htmlComboBox;
    }

    /**
     * generateur HTML sans utilisé de frameworks
     *
     * @return string HTML généré
     */
    public function generatePlainHTMLSelectField(): string {
        $htmlComboBox = '<label for="'.$this->name.'">'.$this->label.' </label> : ';
        $htmlComboBox .= '<select name="'.$this->name.'" id="'.$this->name.'">';
        $htmlComboBox .= $this->generateOptions();
        $htmlComboBox .= '</select>';
        return $htmlComboBox;
    }

    /**
     * generateur HTML utilisant le framworks choisie dans $this->frameWorksName
     *
     * @return string HTML généré
     */
    public function generateSelectField(): string {
        $methodName = 'generate'.ucfirst($this->frameWorksName).'SelectField';
        if(method_exists($this, $methodName)) {
            return $this->$methodName();
        } else {
            return $this->generatePlainHTMLSelectField();
        }
    }
}