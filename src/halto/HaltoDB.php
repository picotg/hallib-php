<?php
namespace uga\hallib\halto;

use uga\hallib\queryDefinition\DataContainer;
use \PDO;
use PDOStatement;
use uga\hallib\queryDefinition\LiteralElement;
use uga\hallib\queryDefinition\QueryTreeElement;
use uga\hallib\search\SearchField;

/**
 * Outil pour l'utilisation des bases de données HalTo
 * 
 * @author Gaël PICOT <gael.picot@univ-grenoble-alpes.fr>
 * 
 * Hallib :
 * Copyright (C) 2022 UGA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 * 
 */

class HaltoDB extends DataContainer {
    protected ?PDO $database = null;

    public function setDatabase(string $filename) {
        if($this->database != null) {}
        try {
            $this->database = new PDO('sqlite:'.$filename);
            $this->database->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
            $this->database->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
            $this->database->setAttribute(PDO::SQLITE_ATTR_OPEN_FLAGS, PDO::SQLITE_OPEN_READONLY);
        } catch(\Exception $e) {
            echo "Impossible d'accéder à la base de données SQLite : ".$e->getMessage();
            die();
        }
    }

    public function prepare(string $query, array $option=[]): PDOStatement {
        return $this->database->prepare($query, $option);
    }

    public function selectLine(int $idLine) {
        $query = 'SELECT * FROM line Where id = :idLine';
        $stmt = $this->database->prepare($query);
        $stmt->execute([
            'idLine' => $idLine,
        ]);
        $line = $stmt->fetch(\PDO::FETCH_ASSOC);
        if($line) {
            return [
                'id' => $line['id'],
                'label' => $line['label'],
                'acronym' => $line['acronym'],
                'type' => $line['type'],
            ];
        } else {
            return null;
        }
    }

    public function selectLineFromElement(int $idElement) {
        $query = 'SELECT line_id FROM line_element_through Where element_id = :element_id';
        $stmt = $this->database->prepare($query);
        $stmt->execute([
            'element_id' => $idElement,
        ]);
        $element = $stmt->fetch(\PDO::FETCH_ASSOC);
        $idLine = $element['line_id'];
        $query = 'SELECT * FROM line Where id = :idLine';
        $stmt = $this->database->prepare($query);
        $stmt->execute([
            'idLine' => $idLine,
        ]);
        $line = $stmt->fetch(\PDO::FETCH_ASSOC);
        if($line) {
            return [
                'id' => $line['id'],
                'label' => $line['label'],
                'acronym' => $line['acronym'],
                'type' => $line['type'],
            ];
        } else {
            return null;
        }
    }

    public function selectElements(int $id_line, ?string $startDate=null, ?string $endDate=null): array {
        if($this->database == null) {
            return [];
        }
        $query = 'SELECT * FROM element JOIN line_element_through on element.id = line_element_through.element_id Where line_element_through.line_id = :line';
        $stmt = $this->database->prepare($query);
        $stmt->execute([
            'line' => $id_line,
        ]);
        $elements = [];
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            if($startDate != null and $row['date_end'] != null and strtotime($row['date_end']) < strtotime($startDate)) {
                continue;
            }
            if($endDate != null and $row['date_start'] != null and strtotime($row['date_start']) < strtotime($endDate)) {
                continue;
            }
            $elements[] = [
                'id' => $row['id'],
                'date_end' => $row['date_end'],
                'date_start' => $row['date_start'],
                'unknown_start' => $row['unknown_start'],
                'hal_id' => $row['hal_id'],
            ];
        }
        return $elements;
    }

    public function createQueryElement(int $idLine, string $fieldName='structId_i'): QueryTreeElement {
        $queryElement = new QueryTreeElement([
            'field' => SearchField::getVarient($fieldName),
        ]);
        $query = 'SELECT * FROM element JOIN line_element_through on element.id = line_element_through.element_id Where line_element_through.line_id = :line';
        $stmt = $this->database->prepare($query);
        $stmt->execute([
            'line' => $idLine,
        ]);
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $queryElement->addElement(new LiteralElement([
                'value' => strval($row['hal_id'])
            ]));
        }
        return $queryElement;
    }

    public function getLines(): array {
        if($this->database == null) {
            return [];
        }
        $stmt = $this->database->query('SELECT * FROM line');
        $lines = [];
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $lines[] = [
                'id' => $row['id'],
                'label' => $row['label'],
                'acronym' => $row['acronym'],
                'type' => $row['type'],
            ];
        }
        return $lines;
    }
}
