<?php
namespace uga\hallib\queryDefinition;

/**
 * Definition d'un QueryElement de type literale
 * 
 * @author Gaël PICOT <gael.picot@univ-grenoble-alpes.fr>
 * 
 * Hallib :
 * Copyright (C) 2022 UGA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 * 
 */

/**
 * valeur seul de requête
 */
class LiteralElement extends QueryElement {
    /**
     * Valeur possible NOT, !, + et -
     *
     * @var string
     */
    protected string $prefix = '';
    /**
     * valeur possible = '?', '*' ou ''
     *
     * @var string
     */
    protected string $truncateChar = '';
    /**
     * Valeur du literal
     *
     * @var string
     */
    protected string $value;
    /**
     * échape ou non les élément de requette
     *
     * @var boolean
     */
    protected bool $escape = false;

    public function getValue() {
        if($this->convertor != null) {
            return $this->convertor::fromHal($this->value);
        }
        return $this->value;
    }

    public function setValue($value) {
        if($this->convertor != null) {
            $this->value = $this->convertor::toHal($value);
        }
        else {
            $this->value = $value;
        }
    }

    public function getStringValue(): string {
        $value = $this->value;
        if($this->field!==null) {
            $valuesTranslator = $this->field->valuesTranslator;
            if($valuesTranslator!==null) {
                if(isset($valuesTranslator[$value])) {
                    $value = $valuesTranslator[$value];
                }
            }
        }
        if($this->escape) {
            $value = str_replace(
                ['+', '-', '&&', '||', '!', '(', ')', '{', '}', '[', ']', '^', '"', '~', '*', '?', ':'],
                [
                    '\+', '\-', '\&&', '\||', '\!', '\(', '\)', '\{', '\}',
                    '\[', '\]', '\^', '\"', '\~', '\*', '\?', '\:'],
                $value);
        }
        if(strpos($value, ' ') !== false) {
            $value = '"'.$value.'"';
        }
        return $this->prefix.$value.$this->truncateChar;
    }
}
