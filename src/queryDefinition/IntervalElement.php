<?php
namespace uga\hallib\queryDefinition;

/**
 * Definition d'un QueryElement de type intervale
 * 
 * @author Gaël PICOT <gael.picot@univ-grenoble-alpes.fr>
 * 
 * Hallib :
 * Copyright (C) 2022 UGA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 * 
 */

class IntervalElement extends QueryElement {
    protected string $minValue = '*';
    protected string $maxValue = '*';
    protected bool $includeMin = true;
    protected bool $includeMax = true;

    public function getMaxValue() {
        if($this->convertor != null) {
            return $this->convertor::fromHal($this->maxValue);
        }
        return $this->maxValue;
    }

    public function setMaxValue($maxValue) {
        if($this->convertor != null) {
            $this->maxValue = $this->convertor::toHal($maxValue);
        }
        else {
            $this->maxValue = $maxValue;
        }
    }

    public function getMinValue() {
        if($this->convertor != null) {
            return $this->convertor::fromHal($this->minValue);
        }
        return $this->minValue;
    }

    public function setMinValue($minValue) {
        if($this->convertor != null) {
            $this->minValue = $this->convertor::toHal($minValue);
        }
        else {
            $this->minValue = $minValue;
        }
    }

    public function getStringValue(): string {
        $stringValue = $this->includeMin?'[':'{';
        $stringValue .= $this->minValue;
        $stringValue .= ' TO '.$this->maxValue;
        $stringValue .= $this->includeMax?']':'}';
        return  $stringValue;
    }
}
