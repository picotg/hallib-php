<?php
namespace uga\hallib\queryDefinition;

/**
 * Definition d'un QueryElement sous forme d'arbre
 * 
 * @author Gaël PICOT <gael.picot@univ-grenoble-alpes.fr>
 * 
 * Hallib :
 * Copyright (C) 2022 UGA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 * 
 */

/**
 * Arbre de valer pour requête
 */
class QueryTreeElement extends QueryElement {
    protected array $elements = [];
    protected string $operator = 'OR';
    protected string $spaceValue = ' ';
    protected static array $readOnly = ['elements'];

    public function addElement(QueryElement $element) {
        array_push($this->elements, $element);
    }

    public function getStringValue(): string {
        return '('.implode($this->spaceValue.$this->operator.$this->spaceValue, $this->elements).')';
    }
}
