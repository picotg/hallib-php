<?php
namespace uga\hallib\queryDefinition\queryTraits;

use uga\hallib\queryDefinition\LiteralElement;
use uga\hallib\queryDefinition\QueryElement;

/**
 * Definition trait pour la gestion des requêt filtre.
 * 
 * @author Gaël PICOT <gael.picot@univ-grenoble-alpes.fr>
 * 
 * Hallib :
 * Copyright (C) 2022 UGA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 * 
 */

trait QueryFilter {

    /**
     * Requête de fitrage.
     *
     * @var array
     */
    protected array $filterQueries = [];

    public function addParametersFilter(array &$parameters) {
        foreach($this->filterQueries as $filterQuery) {
            array_push($parameters, ['fq', $filterQuery]);
        }
    }

    /**
     * Ajoute un filtre (fq).
     *
     * @param QueryElement $newFilterQuery
     * @return void
     */
    public function addFilterQuery(QueryElement $newFilterQuery) {
        array_push($this->filterQueries, $newFilterQuery);
    }

    /**
     * Ajoute un filtre
     *
     * @param QueryElement|string $query
     * @param FieldVarient|string $field
     * @return void
     */
    public function addFilter($value, $field=null) {
        if(is_string($field)) {
            $field = $this->fieldClass::getVarient($field);
        }
        if(is_string($value)) {
            $value = new LiteralElement(['field' => $field, 'value' => $value]);
        } elseif ($field!==null) {
            $value->field = $field;
        }
        $this->addFilterQuery($value);
    }

    public function initTraitFilter() {
        $this->addToReadOnly('filterQueries');
    }
}
