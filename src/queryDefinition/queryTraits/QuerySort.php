<?php
namespace uga\hallib\queryDefinition\queryTraits;

use uga\hallib\queryDefinition\FieldVarient;

/**
 * Definition trait pour la gestion du tri.
 * 
 * @author Gaël PICOT <gael.picot@univ-grenoble-alpes.fr>
 * 
 * Hallib :
 * Copyright (C) 2022 UGA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 * 
 */

trait QuerySort {
    /**
     * Champs de trie.
     *
     * @var FieldVarient|null
     */
    protected ?FieldVarient $sort = null;

    /**
     * Direction asc du trie.
     * (si faux direction desc)
     *
     * @var boolean
     */
    protected bool $ascSortDirection = false;

    public function addParametersSort(array &$parameters) {
        if(!isset($this->useCursor) || !$this->useCursor) {
            if($this->sort !== null) {
                array_push($parameters, ['sort', $this->sort->currentName.' '.($this->ascSortDirection?'asc':'desc')]);
            }
        }
    }

}
