<?php
namespace uga\hallib\queryDefinition\queryTraits;

/**
 * Definition trait pour la gestion des champs de retour
 * 
 * @author Gaël PICOT <gael.picot@univ-grenoble-alpes.fr>
 * 
 * Hallib :
 * Copyright (C) 2022 UGA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 * 
 */

trait QueryReturnedFields {

    /**
     * liste des champs retourné par la requête (fl).
     *
     * @var array
     */
    protected array $returnedFields = [];

    /**
     * Ajout d'un champs à retourner.
     *
     * @param FieldVarient|string $field
     * @return void
     */
    public function addReturnedField($field) {
        if(is_string($field)) {
            $field = $this->fieldClass::getVarient($field);
        }
        array_push($this->returnedFields, $field);
    }

    /**
     * ajoute plusieurs champs à retourner.
     *
     * @param array $fields tableau de la liste des noms de champs à retourner à ajouter.
     * @return void
     */
    public function addReturnedFields(array $fields) {
        foreach($fields as $field) {
            $this->addReturnedField($field);
        }
    }

    /**
     * Vide la liste des champs à retourner.
     *
     * @return void
     */
    public function emptyReturnedFields() {
        $this->returnedFields = [];
    }

    /**
     * Obtenir la liste des champs retourner et leur nom actuel.
     *
     * @return array
     */
    public function getResultFieldsTranslator(): array {
        $fieldTranslators = [];
        foreach($this->returnedFields as $fieldTranslator) {
            $fieldTranslators[$fieldTranslator->name] = $fieldTranslator->currentName;
        }
        return $fieldTranslators;
    }

    public function addParametersReturnedFields(array &$parameters) {
        array_push($parameters, ['fl', $this->allReturnedField?'*':implode(',', $this->returnedFields)]);
    }

    public function initTraitReturnedFields() {
        $this->addToReadOnly('returnedFields');
    }
}
