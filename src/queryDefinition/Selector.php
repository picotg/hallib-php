<?php
namespace uga\hallib\queryDefinition;

use stdClass;

/**
 * Definition de la class Selector permettant de choisir des element parmis une liste.
 * 
 * @author Gaël PICOT <gael.picot@univ-grenoble-alpes.fr>
 * 
 * Hallib :
 * Copyright (C) 2022 UGA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 * 
 */


/**
 * Permet choisir un élément possible parmis ceux disponible dans un référentiel
 */
abstract class Selector extends DataContainer {
    use DataDirectoryAccess;
    /**
     * donnée représentant le résultat d'une requête sur un reférenciel hal
     * 
     * @var array
     */
    protected array $data = [];
    /**
     * Donnée brute
     *
     * @var stdClass
     */
    protected stdClass $rowData;
    /**
     * Utilise les fichier de donner
     * 
     * si non execute une requête pour avoir les données
     *
     * @var boolean
     */
    protected bool $useFileData = true;
    /**
     * liste des fichier de donnée avec leur URL pour des mise à jours
     *
     * @var array
     */
    protected static array $fileDataURL = [];
    /**
     * liste des champs exclut de la gestion des donnée automatique.
     *
     * @var array
     */
    protected static array $excludeList = ['fileDataURL'];
    /**
     * Liste des proprièté accessible en lecture seul.
     *
     * @var array
     */
    protected static array $readOnly = ['rowData', 'data'];

    /**
     * retourne un tableau de value, label et sub pour généré un selecteur.
     * Sub étant un tableau de sous élément (récursif).
     *
     * @return array
     */
    abstract public function getValueLabel(): array;

    /**
     * extraction des donnée et création du tableau this->data
     *
     * @param string|null $fileName
     * @return void
     */
    abstract public function extractData(?string $fileName=null);

    /**
     * Calcul et garde l'intersection des deux selecteurs
     *
     * @param Selector $other deusième selecteur
     * @return void
     */
    public function intersect(Selector $other) {
        $finalDocs = [];
        foreach($other->data as $doc) {
            if(in_array($doc, $this->data)) {
                array_push($finalDocs, $doc);
            }
        }
        $this->data = $finalDocs;
    }

    /**
     * Calcul et garde la soustraction avec un autre selecteur
     *
     * @param Selector $other deusième selecteur
     * @return void
     */
    public function minus(Selector $other) {
        foreach($other->data as $doc) {
            if(in_array($doc, $this->data)) {
                \array_splice($this->data, array_search($doc, $this->data), 1);
            }
        }
    }

    /**
     * Calcul et garde l'intersection des deux ensemble de donnée
     *
     * @param Selector $other deusième selecteur
     * @return void
     */
    public function add(Selector $other) {
        foreach($other->data as $doc) {
            if(!in_array($doc, $this->data)) {
                array_push($this->data, $doc);
            }
        }
    }

    /**
     * mise à jours du fichier de données
     *
     * @return void
     */
    public static function updateFileData() {
        foreach(static::$fileDataURL as $fileName => $dataURL) {
            $content = file_get_contents($dataURL);
            $dataFile = static::getDataDirectory().$fileName;
            file_put_contents($dataFile, $content);
        }
    }
}
