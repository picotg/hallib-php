<?php
namespace uga\hallib\queryDefinition;

/**
 * Definition du DataSet FieldType permettant de créé et gérer les type de champs de requête hal
 * 
 * @author Gaël PICOT <gael.picot@univ-grenoble-alpes.fr>
 * 
 * Hallib :
 * Copyright (C) 2022 UGA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 * 
 */

/**
 * Gestion des type de champs api Hal.
 */
class FieldType extends DataSet {
    protected static array $instances = [];
    /**
     * nom du type de champs Hal.
     *
     * @var string
     */
    protected string $key;
    /**
     * suffix du type de champs Hal.
     *
     * @var string
     */
    protected string $suffix;
    /**
     * si le type de champs Hal est detiner à pouvoir être afficher.
     *
     * @var boolean
     */
    protected bool $display;
    /**
     * si le type de champs Hal est utilisable pour les facette.
     *
     * @var boolean
     */
    protected bool $facette;
    /**
     * si le type de champs Hal est utilisable pour les recherche.
     *
     * @var boolean
     */
    protected bool $search;
    /**
     * si le type de champs Hal est utilisable pour les tries.
     *
     * @var boolean
     */
    protected bool $sort;
    /**
     * List de tous les convertisseurs
     *
     * @var array
     */
    protected array $convertors = [];

    public function addConvertor(string $convertor) {
        $this->convertors[$convertor::NAME] = $convertor;
    }

    /**
     * créé les liste de tous les type.
     *
     * @return void
     */
    public static function createList() {
        $data = json_decode(file_get_contents(dirname(__FILE__, 3).'/data/dataTypes.json'));
        foreach(get_object_vars($data) as $key => $fieldTypeProperties) {
            $fieldTypeProperties->key = $key;
            static::$instances[$key] = new FieldType($fieldTypeProperties);
            if($key == 'tdate') {
                static::$instances[$key]->addConvertor(DateTimeConvertor::class);
            }
        }
    }
}

FieldType::createList();