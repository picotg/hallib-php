<?php
namespace uga\hallib\queryDefinition;

/**
 * Definition de la classe abstraite Convertor qui permet de définir un convertisseur
 * de valeur depuit et vers Hal.
 * 
 * @author Gaël PICOT <gael.picot@univ-grenoble-alpes.fr>
 * 
 * Hallib :
 * Copyright (C) 2022 UGA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 * 
 */

 abstract class Convertor {
    /**
     * type you want
     */
    public const NAME = '';

    /**
     * Convert Hal strig to 
     *
     * @param string $inputValue
     * @return mixed
     */
    static abstract function fromHal(string $inputValue);

    /**
     * Convert object of APPLYING_TYPE to a Hal string value
     *
     * @param mixed $inputValue
     * @return string
     */
    static abstract function toHal($inputValue): string;
}
