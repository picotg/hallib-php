<?php
namespace uga\hallib\queryDefinition;

/**
 * Definition de la class DataSet permettant de créé et géré un set objet DataContainer
 * 
 * @author Gaël PICOT <gael.picot@univ-grenoble-alpes.fr>
 * 
 * Hallib :
 * Copyright (C) 2022 UGA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 * 
 */

/**
 * Set de données de type data container
 */
class DataSet extends DataContainer {
    /**
     * Liste des instences.
     *
     * @var array
     */
    protected static array $instances = [];

    /**
     * Sérialse seulement la cléf.
     *
     * @return string|int clef pour la sérialisation
     */
    public function __serialize(): array {
        return [$this->key];
    }

    /**
     * Désérialise.
     *
     * @param string|int $key clef pour obtenir la clef
     * @return void
     */
    public function __unserialize(array $data): void {
        $key = $data[0];
        $this->fromArray(static::getInstance($key)->toArray());
    }

    /**
     * Obtenir une instance.
     *
     * @param string|int $key clef pour obtenir la clef
     * @return DataContainer
     */
    public static function getInstance($key): DataContainer {
        if(array_key_exists($key, static::$instances)) {
            return static::$instances[$key];
        }
        else {
            return null;
        }
    }

    /**
     * Ajouté une nouvelle instance
     *
     * @param DataContainer $newInstance nouvelle instance à ajouter
     * @param null|string|int $key clef si besoin
     * @return void
     */
    public static function addToInstances(DataContainer $newInstance, $key=null) {
        if($key==null) {
            array_push(static::$instances, $newInstance);
        } else {
            static::$instances[$key] = $newInstance;
        }
    }

    /**
     * initialisation depuis un tableau.
     * Peut-être un tableau qui ne contient que la clef d'un élément.
     *
     * @param array $ar tableau pour l'initialisation
     * @return void
     */
    public function fromArray(array $ar) {
        if(isset($ar[0])&&count(get_object_vars($ar[0])) == 1) {
            $ar = static::getInstance($ar[0]->key)->toArray;
        }
        parent::fromArray($ar);
    }

    /**
     * transformation en tableau.
     *
     * @return array tableau contenant uniquement la clef
     */
    public function toArray(): array {
        return [$this->key];
    }
}