<?php
namespace uga\hallib\queryDefinition;

use stdClass;

/**
 * Definition de la class Result permettant d'accédé à un résultat
 * 
 * @author Gaël PICOT <gael.picot@univ-grenoble-alpes.fr>
 * 
 * Hallib :
 * Copyright (C) 2022 UGA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 * 
 */


/**
 * Un résultat de requête
 */
class Result {
    /**
     * tableau associant le nom de champs d'une version au nom de champs courrant dans Hal.
     *
     * @var array
     */
    private array $fieldsTranslator;
    /**
     * donnée brute renvoyer par Hal.
     *
     * @var stdClass
     */
    private stdClass $rowData;
    /**
     * class des champs accépté paour la requête.
     *
     * @var string
     */
    protected string $fieldClass;

    public function getRowData(): stdClass {
        return $this->rowData;
    }

    /**
     * Constructeur
     *
     * @param stdClass $rowData
     * @param array $fieldsTranslator
     */
    public function __construct(stdClass $rowData, array $fieldsTranslator, string $fieldClass) {
        $this->fieldClass = $fieldClass;
        $this->rowData = $rowData;
        $this->fieldsTranslator = $fieldsTranslator;
    }

    /**
     * Obtenir une version JSON du résultat
     * accessible par $this->json
     *
     * @return string
     */
    public function getJSON(): string {
        $result = [];
        if($this->rowData != null) {
            foreach($this->fieldsTranslator as $fieldName => $currentFieldName) {
                $value = isset($this->rowData->$currentFieldName)?$this->rowData->$currentFieldName:'';
                if($value!==null) {
                    $result[$fieldName] = $value;
                }
            }
        }
        return json_encode($result);
    }

    /**
     * Accéder à une donné
     *
     * @param [type] $name
     */
    public function __get($name) {
        if($name=='json') {
            return $this->getJSON();
        }
        $currentName = $this->fieldsTranslator[$name];
        $value = $this->rowData->$currentName;
        $valuesTranslator = $this->fieldClass::getVarient($name)->valuesTranslator;
        if($valuesTranslator!==null) {
            if(array_search($value, $valuesTranslator)) {
                $value = array_search($value, $valuesTranslator);
            }
        }
        return $value;
    }

    public function __isset($name) {
        $currentName = $this->fieldsTranslator[$name];
        return isset($this->rowData->$currentName)||$name=='json';
    }
}
