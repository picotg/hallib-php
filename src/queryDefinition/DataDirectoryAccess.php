<?php
namespace uga\hallib\queryDefinition;

/**
 * Definition du trait DataDirectoryAccess permettant l'accés au répertoir de donnée correspondant au répértoire dataDirectory
 * 
 * @author Gaël PICOT <gael.picot@univ-grenoble-alpes.fr>
 * 
 * Hallib :
 * Copyright (C) 2022 UGA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 * 
 */

/**
 * Trait permettant l'accés au répertoir de donnée correspondant au répértoire dataDirectory
 */
trait DataDirectoryAccess {
    /**
     * directory for data file
     *
     * @var string
     */
    protected static string $dataDirectory;

    /**
     * Accéde au chemain du répértoire des données
     *
     * @return string Chemain du répértoire des données
     */
    public static function getDataDirectory(): string {
        return dirname(__file__, 3).DIRECTORY_SEPARATOR.'data'.DIRECTORY_SEPARATOR.static::$dataDirectory.DIRECTORY_SEPARATOR;
    }
 }