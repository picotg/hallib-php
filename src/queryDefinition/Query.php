<?php
namespace uga\hallib\queryDefinition;

/**
 * Definition de la class Query permettant de construire une requête
 * 
 * @author Gaël PICOT <gael.picot@univ-grenoble-alpes.fr>
 * 
 * Hallib :
 * Copyright (C) 2022 UGA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 * 
 */

/**
 * transforme une URL et un tableau de paramétre en URL avec paramètre get.
 *
 * @param string $url
 * @param array $parameters
 * @return string
 */
function encodeParameter(string $url, array $parameters): string {
    $finalURL = $url.'?';
    $encodedParameters = [];
    foreach($parameters as $parameter) {
        array_push($encodedParameters, $parameter[0].'='.urlencode($parameter[1]));
    }
    $finalURL .= implode('&', $encodedParameters);
    return $finalURL;
}


/**
 * Représente un requête
 */
abstract class Query extends DataContainer {
    /**
     * URL d'acces API Hal.
     *
     * @var string
     */
    protected static string $baseURL;

    /**
     * Modèle des complément d'URL.
     *
     * @var string
     */
    protected static string $URLComplementPattern = '';

    /**
     * class des champs accépté paour la requête.
     *
     * @var string
     */
    protected static string $FieldClass;

    /**
     * requête de base (q=).
     *
     * @var QueryElement
     */
    protected QueryElement $baseQuery;

    /**
     * format de la réponse.
     *
     * @var string
     */
    protected string $format = 'json';

    /**
     * nombre maximum de résultat (paramétre rows de la requête).
     *
     * @var integer
     */
    protected int $rows = 100;

    /**
     * Class de la reponse
     *
     * @var string
     */
    protected string $responseClass = Response::class;

    /**
     * renvoie tous les champs
     *
     * @var boolean
     */
    protected bool $allReturnedField = false;

    /**
     * Si différent de null remplace la totalité de la requête.
     *
     * @var string
     */
    protected ?string $customQuery = null;

    /**
     * Liste des proprièté accessible en lecture seul.
     *
     * @var array
     */
    protected static array $readOnly = [
        'baseURL',
    ];

    public function addToReadOnly(string $value) {
        array_push(static::$readOnly, $value);
    }

    public function __construct($values=null) {
        parent::__construct($values);
        foreach(get_class_methods(static::class) as $method) {
            $initPrefix = "initTrait";
            if(substr($method, 0, strlen($initPrefix)) === $initPrefix) {
                $this->$method();
            }
        }
    }

    /**
     * obtenir la class des champs de la requête
     *
     * @return string
     */
    public static function getFieldClass(): string {
        return static::$FieldClass;
    }

    /**
     * Obtenir la requête sous forme de string.
     *
     * @return string la requête
     */
    public function getStringValue(): string {
        if($this->customQuery !== null) {
            return $this->customQuery;
        }
        $parameters = [
            ['q', $this->baseQuery],
            ['wt', $this->format],
            ['rows', $this->rows],
        ];
        $complementURL = static::$URLComplementPattern;
        foreach(get_class_methods(static::class) as $method) {
            $addParameters = "addParameters";
            if(substr($method, 0, strlen($addParameters)) === $addParameters) {
                $this->$method($parameters);
            }
            $buildComplement = "buildComplement";
            if(substr($method, 0, strlen($buildComplement)) === $buildComplement) {
                $this->$method($complementURL);
            }
        }
        $query = static::$baseURL.$complementURL;
        return encodeParameter($query, $parameters);
    }
}
