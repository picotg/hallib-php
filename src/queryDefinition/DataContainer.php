<?php
namespace uga\hallib\queryDefinition;

use ReflectionProperty;
use TypeError;
use stdClass;

/**
 * Definition de la class DataContainer permettant de créé des objet gérant l'accés à des données
 * 
 * @author Gaël PICOT <gael.picot@univ-grenoble-alpes.fr>
 * 
 * Hallib :
 * Copyright (C) 2022 UGA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 * 
 */

/**
 * Container pour données
 */
class DataContainer {
    /**
     * liste des champs réserver au fonctionnement
     */
    public const SYSTEM_FIELD = [
        'excludeList',
        'readOnly',
    ];

    /**
     * Liste des proprièté accessible en lecture seul.
     *
     * @var array
     */
    protected static array $readOnly = [];
    /**
     * liste des champs exclut de la gestion des donnée automatique.
     *
     * @var array
     */
    protected static array $excludeList = [];

    public function __serialize(): array {
        return $this->toArray();
    }

    public function __unserialize(array $data): void  {
        $this->fromArray($data);
    }
    /**
     * Constructeur.
     *
     * @param [type] $values peut être ou array ou stdObject
     */
    public function __construct($values=null) {
        if($values!=null) {
            if(is_array($values)) {
                $this->fromArray($values);
            } else {
                $this->fromObject($values);
            }
        }
    }

    /**
     * initialisation depuis un objets.
     *
     * @param stdClass $obj
     * @return void
     */
    public function fromObject(stdClass $obj) {
        $this->fromArray(get_object_vars($obj));
    }

    /**
     * initialisation depuis un tableau.
     *
     * @param array $ar
     * @return void
     */
    public function fromArray(array $ar) {
        foreach($ar as $key => $value) {
            if($value!==null&&property_exists(get_class($this), $key)&&!in_array($key, static::SYSTEM_FIELD)&&!in_array($key, static::$excludeList)) {
                if(isset($this->$key)&&is_bool($this->$key)&&is_string($value)) {
                    $value = ($value==='true')?true:false;
                }
                $rp = new ReflectionProperty(get_class($this), $key);
                if($rp->getType()->getName()=='array'&&is_object($value)) {
                    $value = get_object_vars($value);
                }
                $this->$key = $value;
            }
        }
    }

    /**
     * transformation en tableau.
     *
     * @return array valeur des données sous forme de tableau
     */
    public function toArray(): array {
        $result = [];
        foreach(get_object_vars($this) as $key => $value) {
            if(!in_array($key, static::$excludeList)&&!in_array($key, static::SYSTEM_FIELD)) {
                $result[$key] = $value;
            }
        }
        return $result;
    }

    /**
     * vérifi si tous les champs sont égaux.
     *
     * @param DataContainer $other l'autre Datacontainer à comparer
     * @return boolean tous les champs égaux
     */
    public function equals(DataContainer $other): bool {
        if(gettype($other) == gettype($this)) {
            foreach(get_object_vars($this) as $key => $value) {
                if($value!=$other->$key) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    /**
     * modifie la valeur d'une donnée.
     *
     * @param string $name noms de la donnée
     * @param [type] $value nouvelle valeur
     */
    public function __set(string $name, $value) {
        if (method_exists($this, 'set'.ucfirst($name))) {
            $methodName = 'set'.ucfirst($name);
            $this->$methodName($value);
        } elseif (!in_array($name, static::$readOnly)) {
            $this->$name = $value;
        }
    }

    /**
     * lecture d'une donnée.
     *
     * @param string $name nom de la donnée.
     * @return 
     */
    public function __get(string $name) {
        if (method_exists($this, 'get'.ucfirst($name))) {
            $methodName = 'get'.ucfirst($name);
            return $this->$methodName();
        } elseif (property_exists($this, $name)) {
            return $this->$name;
        } else {
            throw new TypeError('champs '.$name.' non exisatant');
        }
    }

    /**
     * Verifie si on peut accédé à la propriété $name
     *
     * @param string $name
     * @return boolean la propriété existe
     */
    public function __isset(string $name) {
        return method_exists($this, 'get'.ucfirst($name))||property_exists($this, $name);
    }
}