<?php
namespace uga\hallib\queryDefinition;

/**
 * Definition de la class QueryElement permettant de représenté un élément d'une requête
 * 
 * @author Gaël PICOT <gael.picot@univ-grenoble-alpes.fr>
 * 
 * Hallib :
 * Copyright (C) 2022 UGA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 * 
 */

/**
 * valeur d'une requête
 */
abstract class QueryElement extends DataContainer {
    /**
     * Type de champs
     *
     * @var FieldVarient|null
     */
    protected ?FieldVarient $field=null;
    /**
     * Valeur possible NOT, !, + et -
     *
     * @var string
     */
    protected string $prefix = '';
    /**
     * nom clef du convertor dans type de champs
     *
     * @var string|null
     */
    protected ?string $convertorName = null;

    public function getConvertor() {
        if($this->field != null && $this->convertorName != null && key_exists($this->convertorName, $this->field->type->convertors)) {
            return $this->field->type->convertors[$this->convertorName];
        }
        return null;
    }

    /**
     * Retourne une version sous forme de chaine de caractère
     *
     * @return string valeur sous forme de chaine de caractère
     */
    abstract public function getStringValue(): string;

    /**
     * retourne la valeur compléte sous forme de string (avec préfixe et nom du champs si nécessaire)
     *
     * @return string préfixe, nom du champs et stringValue
     */
    public function __toString() {
        $fieldame = ($this->field!==null)?$this->field->name.':':'';
        return $this->prefix.$fieldame.$this->getStringValue();
    }
}