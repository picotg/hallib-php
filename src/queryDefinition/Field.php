<?php
namespace uga\hallib\queryDefinition;

/**
 * Definition du DataSet FieldType permettant de créé et gérer les champs de requête hal
 * 
 * @author Gaël PICOT <gael.picot@univ-grenoble-alpes.fr>
 * 
 * Hallib :
 * Copyright (C) 2022 UGA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 * 
 */

/**
 * Description d'un champs
 */
class Field extends DataSet {
    /**
     * utilise le trait permetant d'accédé au data.
     */
    use DataDirectoryAccess;
    /**
     * Tableau des instances.
     *
     * @var array
     */
    protected static array $instances = [];

    /**
     * liste des pattern de champs dynamique.
     *
     * @var array
     */
    protected static array $dynamicPatterns = [];
    /**
     * liste de toutes les varientes disponibles.
     *
     * @var array
     */
    protected static array $varientFullList = [];

    /**
     * le champ est-il dynamique.
     *
     * @var boolean
     */
    protected bool $dynamic = false;
    /**
     * Description du champs.
     *
     * @var string
     */
    protected string $description;
    /**
     * Nom du champs.
     *
     * @var string
     */
    protected string $name;
    /**
     * Variente du champs.
     *
     * @var array
     */
    protected array $varients = [];

    /**
     * constructeur.
     *
     * @param [type] $values
     */
    protected function __construct($values=null) {
        parent::__construct($values);
    }

    /**
     * Créé la liste du des champs.
     *
     * @return void
     */
    public static function createList() {
        $description = static::getDataDirectory().'fields_description.json';
        $descriptionData = json_decode(file_get_contents($description));
        foreach(get_object_vars($descriptionData) as $name => $fieldProperties) {
            $fieldProperties->name = $name;
            static::addToInstances(new Field($fieldProperties), $name);
            foreach($fieldProperties->variant as $varient) {
                if($fieldProperties->dynamic) {
                    array_push(static::$dynamicPatterns, $varient->name);
                }
                $varient->FieldClass = static::class;
                $varient->type = FieldType::getInstance($varient->type);
                $varient->field = static::getInstance($name);
                $newVariant = new FieldVarient($varient);
                array_push(static::$instances[$name]->varients, $newVariant);
                static::$varientFullList[$varient->name] = $newVariant;
            }
        }
    }

    /**
     * obtien la list des varient.
     *
     * @return array
     */
    public static function getVarientList(): array {
        return static::$varientFullList;
    }

    /**
     * Obtenir un varient.
     *
     * @param string $name
     * @param string $typeName
     * @return FieldVarient|null
     */
    public static function getVarient(string $name, string $typeName=''): ?FieldVarient {
        if($typeName==='') {
            if(isset(static::$varientFullList[$name])) {
                return static::$varientFullList[$name];
            } else {
                foreach(static::$dynamicPatterns as $pattern) {
                    $phpPattern = '/'.str_replace('*', '(\\w*)', $pattern).'$/';
                    if(preg_match($phpPattern, $name)!=false) {
                        $finded = static::getVarient($pattern);
                        $newVarient = new FieldVarient($finded->toArray());
                        $newVarient->name = $name;
                        $newVarient->currentName = $name;
                        return $newVarient;
                    }
                }
                return null;
            }
        } elseif(isset(static::$instances[$name])&&static::$instances[$name]) {
            foreach(static::$instances[$name]->varients as $variant) {
                if($variant->type==FieldType::getInstance($typeName)) {
                    return $variant;
                }
            }
        } else {
            return null;
        }
    }
}
