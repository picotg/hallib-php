<?php
namespace uga\hallib\queryDefinition;

/**
 * Definition du DataSet FieldType permettant de créé et gérer les type de champs de requête hal
 * 
 * @author Gaël PICOT <gael.picot@univ-grenoble-alpes.fr>
 * 
 * Hallib :
 * Copyright (C) 2022 UGA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 * 
 */

/**
 * le varient d'un champs
 */
class FieldVarient extends DataContainer {
    protected string $FieldClass;
    /**
     * type de champs.
     *
     * @var FieldType
     */
    protected FieldType $type;
    /**
     * champs.
     *
     * @var Field
     */
    protected Field $field;
    /**
     * nom du champs dans Hal au moment de la création du fichier.
     *
     * @var string
     */
    protected string $name = '';
    /**
     * Nom du champs dans la version actuelle de Hal.
     *
     * @var string
     */
    protected string $currentName = '';
    /**
     * est indéxé dans Hal.
     *
     * @var boolean
     */
    protected bool $indexed = false;
    /**
     * est stoqué dans Hal.
     *
     * @var boolean
     */
    protected bool $stocked = false;
    /**
     * retourne un tableau de valeurs.
     *
     * @var boolean
     */
    protected bool $multiplevalue = false;
    /**
     * Si non null alors les valeur doivent être modifier.
     * ancienne => nouvelle
     *
     * @var array|null
     */
    protected ?array $valuesTranslator = null;

    /**
     * Sérialise.
     *
     * @return string
     */
    public function __serialize(): array {
        return [
            'type' => $this->FieldClass,
            'name' => $this->name,
        ];
    }

    /**
     * Désérialise.
     *
     * @param [type] $value
     * @return void
     */
    public function __unserialize(array $data): void {
        if($data['type']::getVarient($data['name'])==null) {
            $data['type']::createList();
        }
        $this->fromArray($data['type']::getVarient($data['name'])->toArray());
    }

    /**
     * valeur alphanumérique.
     *
     * @return string
     */
    public function __toString(): string {
        return $this->currentName;
    }
}