<?php
namespace uga\hallib\sword;

use SimpleXMLElement;

/**
 * Utilitaires pour la manipulation de fichier TEI
 * 
 * @author Gaël PICOT <gael.picot@univ-grenoble-alpes.fr>
 * 
 * Hallib :
 * Copyright (C) 2022 UGA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 * 
 */

function simplexml_insert_before(SimpleXMLElement $insert, SimpleXMLElement $target) {
    $target_dom = dom_import_simplexml($target);
    $insert_dom = $target_dom->ownerDocument->importNode(dom_import_simplexml($insert), true);
    if ($target_dom->previousSibling) {
        return $target_dom->parentNode->insertBefore($insert_dom, $target_dom->previousSibling);
    } else {
        return $target_dom->parentNode->appendChild($insert_dom);
    }
}

/**
 * Class représentant un fichier TEI Hal.
 */
class TEIFile {
    /**
     * contenu XML du document
     *
     * @var SimpleXMLElement
     */
    private SimpleXMLElement $value;

    /**
     * costructor
     *
     * @param string $value contenue du fichier
     */
    public function __construct(string $value) {
        $this->value = new SimpleXMLElement($value);
        $this->value->registerXPathNamespace('tei', 'http://www.tei-c.org/ns/1.0');
    }

    /**
     * get direct accés to XML document.
     *
     * @return SimpleXMLElement
     */
    public function getXMLValue(): SimpleXMLElement {
        return $this->value;
    }

    /**
     * Get string value of the content XML.
     *
     * @return string
     */
    public function getValue(): string {
        return $this->value->asXML();
    }

    /**
     * Change the DOI in the XML value
     *
     * @param string $newDOI
     * @return void
     */
    public function changeDoi(string $newDOI) {
        $DOINode = $this->value->xpath("/tei:TEI/tei:text/tei:body/tei:listBibl/tei:biblFull/tei:sourceDesc/tei:biblStruct/tei:idno[@type='doi']");
        if(count($DOINode) > 0) {
            $DOINode[0][0] = $newDOI;
        } else {
            $RefNode = $this->value->xpath("/tei:TEI/tei:text/tei:body/tei:listBibl/tei:biblFull/tei:sourceDesc/tei:biblStruct/tei:ref");
            if(count($RefNode) > 0) {
                $insert = new SimpleXMLElement('<idno type="doi">'.$newDOI.'</idno>');
                simplexml_insert_before($insert, $RefNode[0]);
            } else {
                $parent = $this->value->xpath("/tei:TEI/tei:text/tei:body/tei:listBibl/tei:biblFull/tei:sourceDesc/tei:biblStruct");
                $DOINode = $parent[0]->addChild('idno', $newDOI);
                $DOINode->addAttribute('type', 'doi');
            }
        }
    }

    /**
     * Supprime l'attribut version du document DOI (parfois nécéssaire selon les versions de l'API SWORD HAL)
     *
     * @return void
     */
    public function delVersionNode() {
        $versionNode = $this->value->xpath("//tei:TEI/@version");
        unset($versionNode[0][0]);
    }

    /**
     * Supprime les auteurs du document DOI (parfois nécéssaire selon les versions de l'API SWORD HAL)
     *
     * @return void
     */
    public function delAuthors() {
        $authorsnNode = $this->value->xpath("//tei:author[@role='aut']");
        foreach ($authorsnNode as $authorNode) {
            unset($authorNode[0]);
        }
    }
}

