<?php
namespace uga\hallib\sword;

use uga\hallib\queryDefinition\DataContainer;

/**
 * Utilitaires pour l'utilisation de l'API sword
 * 
 * @author Gaël PICOT <gael.picot@univ-grenoble-alpes.fr>
 * 
 * Hallib :
 * Copyright (C) 2022 UGA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 * 
 */

class Sword extends DataContainer {
    /**
     * URL de l'API.
     *
     * @var string
     */
    protected string $baseUrl = "https://api.archives-ouvertes.fr/sword/";
    /**
     * Portail Hal.
     *
     * @var string
     */
    protected string $portail = '';
    /**
     * Code de la réponse http après envoie à l'API SWORD de HAl.
     *
     * @var integer
     */
    protected int $httpResponseCode=0;
    /**
     * Retour info de curl.
     *
     * @var array
     */
    protected array $info;

    /**
     * modifier de portail.
     *
     * @param string $newPortail
     * @return void
     */
    public function setPortail(string $newPortail) {
        if(substr($newPortail, -1) !== '/') {
            $newPortail .= '/';
        }
        $this->portail = $newPortail;
    }

    /**
     * Accés à l'URL complet (baseulr + portail)
     *
     * @return string
     */
    public function getFullURL(): string {
        return $this->baseUrl.$this->portail;
    }

    /**
     * Enoyer un fichier TEI
     *
     * @param TEIFile $file fichier TEI à envoyer
     * @param string $username nom d'utilisateur pour l'authentification
     * @param string $password mot de passe pour l'authentification
     * @param string $halId_s halid de la publie à modifier
     * @return string resultat renvoyer par l'API
     */
    public function sendTEI(TEIFile $file, string $username, string $password, string $halId_s): string {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $this->getFullURL().$halId_s);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($curl, CURLOPT_USERPWD, "$username:$password");
        curl_setopt($curl, CURLOPT_POSTFIELDS, $file->getValue());
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            "Packaging: http://purl.org/net/sword-types/AOfr",
            "Content-Type: text/xml",
        ));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $resp = curl_exec($curl);
        $this->info = curl_getinfo($curl);
        $this->httpResponseCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);
        return $resp;
    }
}
