<?php
namespace uga\hallib\ref\doctype;

/**
 * class permetant de selectionné un type de document parmis le reférenciel hal doctype.
 * 
 * @author Gaël PICOT <gael.picot@univ-grenoble-alpes.fr>
 * 
 * Hallib :
 * Copyright (C) 2022 UGA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 * 
 */

function listToDocs(array $docs, ?string $parentValue=null) {
    $result = [];
    foreach($docs as $doc) {
        $newValue = (object)['str' => $doc->str];
        if($parentValue!==null) $newValue->parent=$parentValue;
        array_push($result, $newValue);
        if(isset($doc->arr)) {
            $result = array_merge($result, listToDocs($doc->arr->doc, $doc->str[0]));
        }
    }
    return $result;
}

use uga\hallib\queryDefinition\Selector;

/**
 * selecteur de type de document parmis le reférenciel hal doctype.
 */
class DocTypeSelector extends Selector {
    protected static array $fileDataURL = [
        'list.json' => 'https://api.archives-ouvertes.fr/ref/doctype',
        'list.en.json' => 'https://api.archives-ouvertes.fr/ref/doctype?lang=en',
    ];
    protected static string $dataDirectory = 'doctype';

    protected string $portail = '';
    protected string $lang = '';

    public function setPortail($portail) {
        $this->portail = $portail;
        $this->extractData();
    }

    public function extractData(?string $fileName=null) {
        if($fileName === null) {
            $fileName = static::getDataDirectory().'list';
            $fileName .= ($this->portail=='')?'':'.'.$this->portail;
            $fileName .= ($this->lang=='')?'':'.'.$this->lang;
            $fileName .= '.json';
        }
        if($this->useFileData) {
            $source = $fileName;
        } else {
            $source = 'https://api.archives-ouvertes.fr/ref/doctype?';
            $source .= ($this->portail=='')?'':'.'.$this->portail.'&';
            $source .= ($this->lang=='')?'':'.'.$this->lang;
        }
        $this->rowData = json_decode(file_get_contents($source));
        $this->data = [];
        $this->data = listToDocs($this->rowData->response->result->doc);
    }

    /**
     * Accesseur pour la liste des labels. (utilisation: $this->valueLabel)
     *
     * @return array liste des labels
     */
    public function getValueLabel(): array {
        $valuesLabels = [];
        foreach($this->data as $instance) {
            $newValue = [
                'value' => $instance->str[0],
                'label' => $instance->str[1]
            ];
            if(isset($instance->parent)) {
                if(!isset($valuesLabels[$instance->parent]['sub'])) {
                    $valuesLabels[$instance->parent]['sub'] = [];
                }
                array_push($valuesLabels[$instance->parent]['sub'], $newValue);
            } else {
                $valuesLabels[$instance->str[0]] = $newValue;
            }
        }
        return array_values($valuesLabels);
    }

    public function __construct() {
    }
}
