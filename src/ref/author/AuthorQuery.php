<?php
namespace uga\hallib\ref\author;

/**
 * Class pour la création de requête de recherche dans le référentiel auteurs de Hal.
 * 
 * @author Gaël PICOT <gael.picot@univ-grenoble-alpes.fr>
 * 
 * Hallib :
 * Copyright (C) 2022 UGA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 * 
 */

use uga\hallib\queryDefinition\Query;
use uga\hallib\queryDefinition\queryTraits\QueryCursor;
use uga\hallib\queryDefinition\queryTraits\QueryFilter;
use uga\hallib\queryDefinition\queryTraits\QueryReturnedFields;
use uga\hallib\queryDefinition\queryTraits\QuerySort;
use uga\hallib\queryDefinition\queryTraits\QueryStart;

class AuthorQuery extends Query {
    use QueryReturnedFields;
    use QueryFilter;
    use QueryCursor;
    use QuerySort;
    use QueryStart;
    protected static string $FieldClass = AuthorField::class;
    protected static string $baseURL = 'https://api.archives-ouvertes.fr/ref/author';
}
