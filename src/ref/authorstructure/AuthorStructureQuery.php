<?php
namespace uga\hallib\ref\authorstructure;

/**
 * Class pour la création de requête de recherche dans le référentiel auteurstructure de Hal.
 * 
 * @author Gaël PICOT <gael.picot@univ-grenoble-alpes.fr>
 * 
 * Hallib :
 * Copyright (C) 2022 UGA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 * 
 */

use uga\hallib\queryDefinition\Query;
use function uga\hallib\queryDefinition\encodeParameter;

class AuthorStructureQuery extends Query {
    protected static string $baseURL = 'https://api.archives-ouvertes.fr/search/authorstructure/';
    protected static array $excludeList = [
        'instence', 'baseQuery', 'rows',
    ];
    /**
     * Class de la reponse
     *
     * @var string
     */
    protected string $responseClass = AuthorStructureResponse::class;
    /**
     * Nom de l'auteur.
     *
     * @var string
     */
    protected string $firstName ='';
    /**
     * Prénom de l'auteur.
     *
     * @var string
     */
    protected string $lastName ='';
    /**
     * Courriel de l'auteur.
     *
     * @var string
     */
    protected string $mail;
    /**
     * Limite les documents produits à cette année.
     *
     * @var integer
     */
    protected int $producedYear;
    /**
     * true pour récupérer les parents (comportement par défault) ; false pour ne pas avoir les parents (plus rapide).
     *
     * @var boolean
     */
    protected bool $getParents;
    /**
     * Si producedDateY_i est fournie, permet d'élargir la recherche à producedDateY_i ± deviation.
     *
     * @var integer
     */
    protected int $deviation;

    public function getStringValue(): string {
        $parameters = [
            ['lastName_t', $this->lastName],
            ['firstName_t', $this->firstName],
        ];
        if(isset($this->mail)) {
            array_push($parameters, ['email_s', $this->mail]);
        }
        if(isset($this->producedYear)) {
            array_push($parameters, ['producedDateY_i', $this->producedYear]);
        }
        if(isset($this->deviation)) {
            array_push($parameters, ['deviation', $this->deviation]);
        }
        if(isset($this->getParents)) {
            array_push($parameters, ['getParents', $this->getParents]);
        }
        return encodeParameter(static::$baseURL, $parameters);
    }


}
