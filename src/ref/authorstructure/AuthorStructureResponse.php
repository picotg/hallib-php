<?php
namespace uga\hallib\ref\authorstructure;

use stdClass;
use uga\hallib\queryDefinition\Response;

/**
 * Definition de la class Response permettant d'accédé à la réponse d'une requête
 * 
 * @author Gaël PICOT <gael.picot@univ-grenoble-alpes.fr>
 * 
 * Hallib :
 * Copyright (C) 2022 UGA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 * 
 */

class AuthorStructureResponse extends Response {
    public function getDocs(): array {
        return $this->data->response->result->org;
    }

    public function getNumFound(): int {
        return count($this->data->response->result->org);
    }
}
