<?php
namespace uga\hallib\ref\journal;

/**
 * Class créant et gérant la liste des champs pour le référentiel des revues de Hal.
 * 
 * @author Gaël PICOT <gael.picot@univ-grenoble-alpes.fr>
 * 
 * Hallib :
 * Copyright (C) 2022 UGA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 * 
 */

use uga\hallib\queryDefinition\Field;

class JournalField extends Field {
    protected static string $dataDirectory='journal';
    protected static array $fieldList = [];
    protected static array $varientFullList = [];
}

JournalField::createList();
