<?php
namespace uga\hallib\ref\domain;

/**
 * class permetant de selectionné un type de document parmis les domaines de hal.
 * 
 * @author Gaël PICOT <gael.picot@univ-grenoble-alpes.fr>
 * 
 * Hallib :
 * Copyright (C) 2022 UGA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 * 
 */

use uga\hallib\queryDefinition\Selector;

class DomainSelector extends Selector {
    protected static string $dataDirectory = 'domain';
    protected ?array $parentsIndex = null;
    protected ?array $codesIndex = null;
    protected int $level = -1;
    protected ?array $selected = null;

    public static function cleanDomainName($domainName) {
        $pattern = '/ \[[-0-9a-zA-Z.]+\]/';
        $replacement = '';
        return preg_replace($pattern, $replacement, $domainName);
    }

    public function buildData() {
        $this->data = [];
        foreach($this->rowData->response->docs as $doc) {
            $correctLevel = ($this->level === -1) || ($this->level == $doc->level_i);
            $select = ($this->selected == null) || in_array($doc->code_s, $this->selected);
            if($correctLevel&&$select) {
                array_push($this->data, $doc);
            }
        }
    }

    public function setLevel (int $level) {
        $this->level = $level;
        $this->buildData();
    }

    public function setSelected (?array $selected) {
        $this->selected = $selected;
        $this->buildData();
    }

    public function extractData(?string $fileName=null) {
        if($fileName === null) {
            $fileName = static::getDataDirectory().'list.json';
        }
        if ($this->useFileData) {
            $this->rowData = json_decode(file_get_contents($fileName));
        } else {
            $query = 'https://api.archives-ouvertes.fr/ref/domain?fl=label_s,code_s,level_i,parent_i,docid,fr_domain_s&rows=1000';
            $this->rowData = json_decode(file_get_contents($query));
        }
        $this->buildData();
    }

    public function getValueLabel(): array {
        $valeLabels = [];
        foreach($this->data as $doc) {
            array_push($valeLabels, [
                'value' => $doc->code_s,
                'label' => static::cleanDomainName($doc->fr_domain_s),
            ]);
        }
        return $valeLabels;
    }

    public function getDomaineName(string $code, bool $useQuery=false): string {
        if($useQuery) {
            $query = 'https://api.archives-ouvertes.fr/ref/domain?fl=fr_domain_s&q=code_s:'.$code;
            $result = json_decode(file_get_contents($query));
            return static::cleanDomainName($result->response->docs[0]->fr_domain_s);
        } else {
            foreach($this->data as $doc) {
                if($doc->code_s === $code) {
                    return static::cleanDomainName($doc->fr_domain_s);
                }
            }
        }
        return '';
    }

    public function code2docid(string $code):?int {
        foreach($this->data as $domain) {
            if($domain->code_s==$code) return $domain->docid;
        }
        return null;
    }

    public function constructCodesIndex() {
        foreach($this->data as $domain) {
            $this->codesIndex[$domain->code_s] = $domain;
        }
    }

    public function code2doc(string $code) {
        if($this->codesIndex==null) $this->constructCodesIndex();
        if(array_key_exists($code, $this->codesIndex)) return $this->codesIndex[$code];
        return null;
    }

    public function constructParentIndex() {
        foreach($this->data as $domain) {
            $this->parentsIndex[$domain->docid] = $domain;
        }
    }

    public function getParents($domain, $inString=false) {
        if(is_string($domain)) $domain = $this->code2doc($domain);
        if(!isset($domain->parent_i)) return [];
        if($this->parentsIndex==null) $this->constructParentIndex();
        $parent = $this->parentsIndex[$domain->parent_i];
        if($inString) {
            return array_merge([$parent->level_i => $parent->code_s], $this->getParents($parent, true));
        } else {
            return array_merge([$parent->level_i => $parent], $this->getParents($parent));
        }
    }

    public function isParent($domain, ?int $parent_i) {
        if($parent_i==null) return false;
        if(!isset($domain->parent_i)) return false;
        if($domain->parent_i==$parent_i) return true;
        foreach($this->data as $domain2) {
            if($domain->parent_i==$domain2->docid) return $this->isParent($domain2, $parent_i);
        }
        return false;
    }

    public function generateJSONData(int $level=1, string $parent='', ?array $ar=null) {
        $result = [];
        $parentDoc = $this->code2doc($parent);
        $parent_i = ($parentDoc!=null&&$parentDoc->level_i<$level)?$parentDoc->docid:null;
        foreach($this->data as $domain) {
            if ($domain->level_i==$level&&($ar==null||in_array($domain->code_s, $ar))&&($parent_i==null||$this->isParent($domain, $parent_i))) {
                array_push($result, ['value' => $domain->code_s, 'name' => static::cleanDomainName($domain->fr_domain_s)]);
            }
        }
        return json_encode($result);
    }
}
