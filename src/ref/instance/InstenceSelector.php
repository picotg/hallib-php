<?php
namespace uga\hallib\ref\instance;

/**
 * class permetant de selectionné un type de document parmis le reférenciel d'instances hal.
 * 
 * @author Gaël PICOT <gael.picot@univ-grenoble-alpes.fr>
 * 
 * Hallib :
 * Copyright (C) 2022 UGA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 * 
 */

use uga\hallib\queryDefinition\Selector;

class InstenceSelector extends Selector {
    protected static string $dataDirectory = 'instance';
    protected bool $sort = true;

    public function extractData(?string $fileName=null) {
        if($fileName === null) {
            $fileName = static::getDataDirectory().'list.json';
        }
        if ($this->useFileData) {
            $this->rowData = json_decode(file_get_contents($fileName));
        } else {
            $query = 'https://api.archives-ouvertes.fr/ref/instance';
            $this->rowData = json_decode(file_get_contents($query));
        }
        $this->data = [];
        if($this->sort) $labelList = [];
        foreach($this->rowData->response->docs as $key => $doc) {
            array_push($this->data, $doc);
            if($this->sort) $labelList[$doc->name] = $key;
        }
        if($this->sort) ksort($labelList, SORT_LOCALE_STRING);
        if($this->sort) {
            $sortedData = [];
            foreach($labelList as $key) {
                array_push($sortedData, $this->data[$key]);
            }
            $this->data = $sortedData;
        }
    }

    public function getValueLabel(): array {
        $valueLabel = [];
        foreach($this->data as $instance) {
            array_push($valueLabel, [
                'value' => $instance->code,
                'label' => $instance->name
            ]);
        }
        return $valueLabel;
    }
}
