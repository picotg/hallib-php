<?php
namespace uga\hallib;

use Iterator;

/**
 * Utilitaires pour l'execution de requête sur l'API Hal
 * 
 * @author Gaël PICOT <gael.picot@univ-grenoble-alpes.fr>
 * 
 * Hallib :
 * Copyright (C) 2022 UGA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 * 
 */

use uga\hallib\queryDefinition\Query;
use uga\hallib\queryDefinition\Result;

/**
 * iterateur sur une requête utilisant les curseurs
 */
class QueryIterator implements Iterator {
    protected Query $query;
    protected array $results = [];
    protected int $index = 0;
    protected bool $lastPage = false;
    private string $name = '';
    private int $offset = 0;
    private bool $needNewPage = false;
    private array $fieldsTranslator;
    private int $nbResult = 0;

    public function getNbResult(): int {
        return $this->nbResult;
    }

    private function _addNextPage() {
        if(!$this->lastPage) {
            $requestResults = new $this->query->responseClass(json_decode(file_get_contents($this->query->stringValue)));
            if($requestResults->isError||$requestResults->numFound===0) {
                $this->nbResult = 0;
                $this->lastPage = true;
                $this->needNewPage = true;
                return;
            } else {
                $this->nbResult = $requestResults->numFound;
            }
            $new_cursor = $requestResults->nextCursorMark;
            if($new_cursor == $this->query->nextCursorMark) {
                $this->lastPage = true;
            } else {
                $this->query->nextCursorMark = $new_cursor;
                $this->offset += count($this->results);
                $this->results = [];
                $this->index = 0;
                foreach($requestResults->docs as $doc) {
                    array_push($this->results, $doc);
                }
            }
        }
    }

    public function __construct(Query $query)
    {
        $this->query = $query;
        $this->fieldsTranslator = $query->resultFieldsTranslator;
    }

    public function getResults() {
        return $this->results;
    }

    public function getName() {
        return $this->name;
    }

    public function getQuery(): Query {
        return $this->query;
    }

    public function rewind(): void {
        if(!$this->query->useCursor) {
            $this->lastPage = true;
            $requestResults = new $this->query->responseClass(json_decode(file_get_contents($this->query->stringValue)));
            if($requestResults->isError||$requestResults->numFound===0) {
                $this->nbResult = 0;
                $this->needNewPage = true;
            } else {
                $this->nbResult = $requestResults->numFound;
                foreach($requestResults->docs as $doc) {
                    array_push($this->results, $doc);
                }
            }
        } else {
            $this->offset = 0;
            $this->query->nextCursorMark = '*';
            $this->results = [];
            $this->_addNextPage();
        }
    }

    public function current() {
        return new Result($this->results[$this->index], $this->fieldsTranslator, $this->query::getFieldClass());
    }

    public function key() {
        return $this->index + $this->offset;
    }

    public function next():void {
        $this->needNewPage = count($this->results) <= $this->index+1;
        $this->index += 1;
        if($this->needNewPage&&!$this->lastPage) {
            if($this->needNewPage) {
                $this->_addNextPage();
            }
        }
    }

    public function valid():bool {
        return isset($this->results[$this->index]);
    }
}
