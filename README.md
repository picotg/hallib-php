# Hallib php

Librairie pour la construction et l'execution de requête Hal

## class de bases

DataContainer : permet de créé de class sérialsable gérant des données.
DataSet : créé un ensseble de DataContainer

## construction et execution d'une requête

### exemple de requête simple

```php
use uga\hallib\search\SearchQuery;
use uga\hallib\QueryIterator;
use uga\hallib\search\SearchField;
use uga\hallib\queryDefinition\LiteralElement;
// Création d'une requête de recherche
$q = new SearchQuery();
// Changement du nombre de résultat demander
$q->rows = 5000;
// Ajout d'une base à la requête (q= dans la requête Hal)
$q->baseQuery = new LiteralElement(['value' => 'japan']);
// ajout champs de retour
$q->addReturnedField(SearchField::getVarient('abstract_s'));
// Execution et parcourt des rsultat
$qi = new QueryIterator($q);
foreach($qi as $doc) {
    echo $doc->abstract_s;
}
```

### exemple de requête utilisant un intérval comme requête

```php
use uga\hallib\search\SearchQuery;
use uga\hallib\QueryIterator;
use uga\hallib\search\SearchField;
use uga\hallib\queryDefinition\LiteralElement;
use uga\hallib\queryDefinition\IntervalElement;
// Création d'une requête de recherche
$q = new SearchQuery();
// Changement du nombre de résultat demander
$q->rows = 5000;
// Ajout d'une base à la requête (q= dans la requête Hal)
$q->baseQuery = new IntervalElement([
    'minValue' => '2000',
    'maxValue' => '2020',
    'field' => SearchField::getVarient('yearDate_s'),
]);
// ajout champs de retour
$q->addReturnedField(SearchField::getVarient('abstract_s'));
// Execution et parcourt des rsultat
$qi = new QueryIterator($q);
foreach($qi as $doc) {
    echo $doc->abstract_s;
}
```

### exemple de requête utilisant une ligne d'une base Halto

Il est possible d'utilisé une base de données [HalTo](https://gricad-gitlab.univ-grenoble-alpes.fr/python_hal/reatribution) pour faire une requête sur l'historique d'une structure.

```php
use uga\hallib\search\SearchQuery;
use uga\hallib\halto\HaltoDB;
// création query
$query = new SearchQuery();
// création base de données HalTo
$haltoDB = new HaltoDB();
// charger la base de donnée halto.db
$haltoDB->setDatabase(dirname(__FILE__)."/test.db");
// créatio d'un QueryTreeElement à partir de l'id de la line 3
$queryElement = $haltoDB->createQueryElement(3);
// utilisation $queryElement comme base de la requête
$query->baseQuery = $queryElement
```

## API SWORD

l'[API SWORD de Hal](https://api.archives-ouvertes.fr/docs/sword) permet de modifier les documents présent dans Hal.

Pour uiliser l'API SWORD, Hallb fournit les classes :
- uga\hallib\sword\Sword pour intéragir avec l'API,
- uga\hallib\sword\TEIFile qui permet de simplifier la manipulation de fichier TEI nécessaire à l'utilisation de l'API.

### Exemple pour le changement de DOI

```php
use uga\hallib\sword\TEIFile;
use uga\hallib\sword\Sword;
//code du portail
$portail = 'saga';
// id hal, en format string, de la publication (correspondant au champs halId_s de l'API de recherche)
$halId_s = 'hal-0010000';
// nom d'utilisateur
$username = 'user';
// mot de passe
$password = 'pass';
// charger le fichier tei.xml
$TEIFile = new TEIFile(file_get_contents('tei.xml'));
// suppression version qui peut entrainer des problème de formatage pour l'API SWORD
$TEIFile->delVersionNode();
// changement de DOI dans le fichier TEI
$TEIFile->changeDoi('nouveauDOI');
// envoie du fichier TEI modifier avec l'API sword
$api = new Sword();
$api->portail = $portail;
$resp = $api->sendTEI($TEIFile, $username, $password, $halId_s);
// récupération des infos de l'execution curl
$info = $api->info;
// recupération du code réponse
$http_code = $api->httpResponseCode;
```

## Test unitaire

lancement en ligne de commande :
./vendor/bin/phpunit

(⚠ En cas d'erreur SSL, ce connecter au VPN.)

## Mise à jour des données

Pour metre à jour les liste de données du repertoir data, il suffit de lancé le script updateData :
```
php updateData.php
```

## Réécriture requête pour mise à jour API

En cas de mise à jour de l'API Hal, HalLib permet de changer le nom et les valeurs possible des champs dans la requête effectuer sans changer le code. Il suffit de rajouter les données nécessaire dans le fichier fields_description.json correspondant au référenciel.

### Changement de nom des champs

Dans les donner de champs, pour chaque variant, il y a la possibilité de modifier le nom "currentName" qui permet de changer le nom dans la requête.

exemple en cas de renommage de abstract_s en nouveauNom_s:

```json
{
    "abstract": {
        [
            {
                "name": "abstract_s",
                "type": "string",
                "indexed": true,
                "stocked": true,
                "multiplevalue": true,
                "currentName": "nouveauNom_s"
            },
            {...}
        ]
    },
    {...}
}
```

### Changement de valeur possible

Par exemple, la valeur "VALID" du champs valid_s du référentiel authorest devenu "PREFERRED". Pour modifier, la requête et ça lecture sans modifier le code, il suffi d'ajouter "valuesTranslator" au variant dan les datas de type "fields_description.x.json".

```json
{
    {...},
    "valid": {
        "variant": [
            {
                "name": "valid_s",
                "type": "string",
                "indexed": true,
                "stocked": true,
                "multiplevalue": false,
                "currentName": "valid_s",
                "valuesTranslator": {
                    "VALID": "PREFERRED"
                }
            }
        ],
    },
    {...}
}
```

# dossier et fichier

## src

source

- src/OneDocQuery.php : execution d'une requête pour le premier résultat renvoyer seulement.
- src/QueryIterator.php : transformation d'une requête en itérateur sur l'ensemble des documents trouver.

## data

données

- data/dataType.json : liste des type de données Hal avec leurs caractèristiques

### anrproject

data pour le référentiel anrproject

- data/anrproject/fields_description.json : champs disponible pour une recherche dans le referentiel

### author

data pour le référentiel author

- data/anrproject/fields_description.json : champs disponible pour une recherche dans le referentiel

### doctype

data pour le référentiel doctype

- data/doctype.list[.instance][.lang].json : fichier list des doctype par instance et lang

### domain

data pour le référentiel domain

- data/anrproject/fields_description.json : champs disponible pour une recherche dans le referentiel
- list.json : list de tous les domaines

### europeanproject

data pour le référentiel europeanproject

- data/anrproject/fields_description.json : champs disponible pour une recherche dans le referentiel

### instance

data pour le référentiel instance

- list.json : list de toutes les instances

### journal

data pour le référentiel journal

- data/anrproject/fields_description.json : champs disponible pour une recherche dans le referentiel

### metadatalist

data pour le référentiel metadatalist

- data/anrproject/fields_description.json : champs disponible pour une recherche dans le referentiel

### search

data pour les recherche général sur Hal

- data/anrproject/fields_description.json : champs disponible pour une recherche

### structure

data pour le référentiel structure

- data/anrproject/fields_description.json : champs disponible pour une recherche dans le referentiel
