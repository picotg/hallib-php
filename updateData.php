<?php
namespace uga\hallibtt;

$urls = [
    'domain' => 'https://api.archives-ouvertes.fr/ref/domain?rows=10000&fl=*',
    'instance' => 'https://api.archives-ouvertes.fr/ref/instance',
    'doctype' => 'https://api.archives-ouvertes.fr/ref/doctype',
    'doctype_en' => 'https://api.archives-ouvertes.fr/ref/doctype?lang=en',
];

function updateList($filename, $content) {
    $dataDirname = dirname(__FILE__).'/data';
    $filename = $dataDirname.'/'.$filename;
    if(file_exists($filename)) {
        unlink($filename);
    }
    if(!is_dir(dirname($filename))) {
        mkdir(dirname($filename), 0777, true);
    }
    file_put_contents($filename, $content);
}

// MàJ list domaines

echo "MàJ list domaines\n";

updateList('domain/list.json', file_get_contents($urls['domain']));

// MàJ list instances

echo "MàJ list instances\n";

updateList('instance/list.json', file_get_contents($urls['instance']));

// MàJ liste doctypes

echo "MàJ list doctypes\n";

updateList('doctype/list.json', file_get_contents($urls['doctype']));
updateList('doctype/list.en.json', file_get_contents($urls['doctype_en']));

$instanceFile = dirname(__FILE__).'/data/instance/list.json';

$instances = json_decode(file_get_contents($instanceFile));

$instanceCodes = [];
foreach($instances->response->docs as $doc) {
    array_push($instanceCodes, $doc->code);
}

$langs = ['' => '', '.en' => '&lang=en'];

foreach($instanceCodes as $instanceCode) {
    $baseURL = 'https://api.archives-ouvertes.fr/ref/doctype?instance_s='.$instanceCode;
    foreach($langs as $lang => $urlSup) {
        echo "MàJ list doctypes pour l'instance $instanceCode en lang ".(($lang!=='')?$lang:'fr')."\n";
        $newFilename = 'doctype/list.'.$instanceCode.$lang.'.json';
        updateList($newFilename, file_get_contents($baseURL.$urlSup));
    }
}
